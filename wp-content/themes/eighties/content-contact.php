<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Eighties
 * @author Justin Kopepasah
 * @since 1.0.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="hp-heading">', '</h1>' ); ?>
	</header><!-- .entry-header -->

        <p class="page-text">
                                <?php the_block( 'perex', array(
                                    'label'         => __( 'Perex' ),
                                    'type'          => 'one-liner',
                                    'apply_filters' => false,
                                ) ); ?>
        </p>
        <div class="page-cols">
                <div class="page-col">
                    <p class="contact-text">
                        
                                <?php the_block( 'contactText', array(
                                    'label'         => __( 'Text' ),
                                    'type'          => 'one-liner',
                                    'apply_filters' => false,
                                ) ); ?>
                        
                    </p>
                    <table class="contact-table">
                        <tr>
                            <th>
                                <span class="home-icon"></span> Adresa
                            </th>
                            <td><?php the_block( 'address', array(
                                    'label'         => __( 'Adresa' ),
                                    'type'          => 'one-liner',
                                    'apply_filters' => false,
                                ) ); ?></td>
                        </tr>

                        <tr>
                            <th>
                                <span class="mail-icon"></span> E-mail
                            </th>
                            <td>
                                <?php $conMail = get_the_block( 'mail', array(
                                    'label'         => __( 'Email' ),
                                    'type'          => 'one-liner',
                                    'apply_filters' => false,
                                ) ); ?>
                                <a href="mailto:<?php print $conMail;?>"><?php print $conMail;?></a></td>
                        </tr>

                        <tr>
                            <th>
                                <span class="mobile-icon"></span> Mobil
                            </th>
                            <td><?php the_block( 'tel', array(
                                    'label'         => __( 'Telefon' ),
                                    'type'          => 'one-liner',
                                    'apply_filters' => false,
                                ) ); ?></td>
                        </tr>
                    </table>
                    
                </div>

                <div class="page-col">
                    <?php the_content(); ?>
                    
                </div>
            </div>
	
        
        
        
</article><!-- #post-## -->
