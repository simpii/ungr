<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Eighties
 * @author Justin Kopepasah
 * @since 1.0.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="hp-heading">', '</h1>' ); ?>
	</header><!-- .entry-header -->
        
<p class="page-text">
    <?php the_block( 'perex', array(
                                    'label'         => __( 'Perex', 'text-domain' ),
                                    'type'          => 'one-liner',
                                    'apply_filters' => false,
                                ) ); ?>
</p>
        <?php echo the_post_thumbnail( 'medium', array(	'class' => "hp-foto") ); ?>
	<div class="hp-text">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'eighties' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
        
        <?php if(is_front_page()){ ?>
        <div class="hp-cta">
                <span class="cta-text">Zaujala Vás moje tvorba? Neváhejte se mi ozvat!</span>
                <a href="/new/kontakt/">Kontaktujte mě</a>
        </div>
        <?php } ?>
</article><!-- #post-## -->
