<?php
/**
 * Template for displaying portfolio content.
 *
 * @package Eighties
 * @author Justin Kopepasah
 * @since 1.0.0
*/
$postData = get_post_custom( get_the_ID() );
$imageGal = wp_get_attachment_image_src( $postData['obrazek_1'][0], 'portfolio-featured' );
?>

<div class="list-cell" id="post-<?php the_ID(); ?>" >
        <h2 class="cell-heading"><?php echo get_the_title( get_the_ID() ); ?></h2>
                    <a href="<?php the_permalink(); ?>" class="cell-image"><img src="<?php print $imageGal[0]; ?>" alt="<?php echo get_the_title( get_the_ID() ); ?>"></a>
	
</div><!-- #post-## -->