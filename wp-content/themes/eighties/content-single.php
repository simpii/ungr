<?php
/**
 * @package Eighties
 * @author Justin Kopepasah
 * @since 1.0.0
 */


$slider = array();
$sliderThumbs = array();
        for($i=1;$i<=15;$i++){
            if(get_field('obrazek_'.$i)){
            $image = get_field('obrazek_'.$i);
            $imageSrc = wp_get_attachment_image_src( $image, 'main-slider' );
            $imageThmb = wp_get_attachment_image_src( $image, 'portfolio-featured' );
            $slider[] = $imageSrc[0];
            $sliderThumbs[] = $imageThmb[0];
            }
        } 
?>

    <div class="gal-main slick-slider">
        <?php if ($slider){ 
            
            foreach($slider as $s){
                print '<div class="slick-slide" style="background-image: url(\''.$s.'\');"></div>';
            }
        }
        ?>
    </div>

    <div class="gal-sidebar">
        <a href="javascript:javascript:history.go(-1)" class="gal-back">Zpět</a>

        <div class="gal-content">
            <h1 class="gal-heading"><?php echo get_the_title(); ?></h1>
            <p class="gal-text"><?php echo get_the_content(); ?></p>
            <div class="gal-nav slick-slider">
                <?php if ($sliderThumbs){ 
            
                    foreach($sliderThumbs as $sT){
                        print '<div class="slick-slide"><img src="'.$sT.'" alt=""></div>';
                    }
                }
                ?>
            </div>
        </div>
    </div>  
