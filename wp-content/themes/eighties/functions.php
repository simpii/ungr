<?php
/**
 * eighties functions and definitions
 *
 * @package Eighties
 * @author Justin Kopepasah
 * @since 1.0.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 760; /* pixels */
}

/**
 * Eighties only works in WordPress 3.6 or later.
 * Let's make sure users do not run into any
 * troubles when trying to activate Eighties on WP
 * versions less than 3.6.
 *
 * @since 1.0.0
*/
if ( version_compare( $GLOBALS['wp_version'], '3.6', '<' ) ) {
	require_once( dirname( __FILE__ ) . '/inc/compatibility.php' );
}

/**
 * Set the theme mods in a global variable. This makes it easier to
 * retrieve in templates and functions.
 */
// $GLOBALS['eighties_theme_mod'] = get_theme_mod( 'eighties' );

if ( ! function_exists( 'eighties_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function eighties_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on eighties, use a find and replace
	 * to change 'eighties' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'eighties', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'main-slider', '1980', '1240', true );
	add_image_size( 'portfolio-featured', '487', '247', true );
        add_image_size( 'subpage-header', 1980, 350, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'eighties' ),
		'social'  => __( 'Social Menu', 'eighties' )
	) );

	// Enable support for Post Formats.
	add_theme_support( 'post-formats', array( 'gallery', 'image', 'video', 'audio', 'status', 'aside', 'link' ) );

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
	) );

	// Add Editor Style
	add_editor_style( 'css/editor.css' );

	// Add shortcodes for the_excerpt
	add_filter( 'the_excerpt', 'do_shortcode' );
}
endif; // eighties_setup
add_action( 'after_setup_theme', 'eighties_setup' );

/**
 * Register a footer and interactive widget area.
 */
function eighties_widgets_init() {
	/**
	 * Set up our interactive sidebar if a user decided
	 * to enable this sidebar via the Customizer.
	*/
	register_sidebar( array(
		'name'          => __( 'Interactive Sidebar', 'eighties' ),
		'id'            => 'eighties-interactive-sidebar',
		'description'   => __( 'This sidebar opens as a toggle on the right side of a users browser window. Note that the toggle feature requires JavaScript in order to function. But no need to worry, a plain sidebar will appear if JavaScript does not work. If empty, the sidebar with not display.', 'eighties' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	// Footer widget area.
	register_sidebar( array(
		'name'          => __( 'Footer', 'eighties' ),
		'id'            => 'eighties-footer',
		'description'   => __( 'Widget area for the footer. If no widgets are provided, this footer will not appear.', 'eighties' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'eighties_widgets_init' );

/**
 * Register Righteous Google font for Eighties
 *
 * @since 1.0.0
 * @return string
*/
function eighties_header_font_url() {
	$font_url = '';
	/*
	 * Translators: If there are characters in your language
	 * that are not supported by Righteous, translate this to
	 * 'off'. Do not translate into your own language.
	*/
	if ( 'off' !== _x( 'on', 'Righteous font: on or off', 'eighties' ) ) {
		$font_url = add_query_arg( 'family', urlencode( 'Righteous' ), "//fonts.googleapis.com/css" );
	}

	return $font_url;
}

/**
 * Register Raleway Google font for Eighties
 *
 * @since 1.0.0
 * @return string
*/
function eighties_headings_font_url() {
	$font_url = '';
	/*
	 * Translators: If there are characters in your language
	 * that are not supported by Raleway, translate this to
	 * 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Varela Round font: on or off', 'eighties' ) ) {
		$font_url = add_query_arg( 'family', 'Varela+Round', "//fonts.googleapis.com/css" );
	}

	return $font_url;
}

/**
 * Register Open Sans Google font for Eighties
 *
 * @since 1.0.0
 * @return string
*/
function eighties_body_font_url() {
	$font_url = '';
	/*
	 * Translators: If there are characters in your language
	 * that are not supported by Open Sans, translate this to
	 * 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Open Sans font: on or off', 'eighties' ) ) {
		$font_url = add_query_arg( 'family', urlencode( 'Open Sans:400italic,700italic,400,700' ), "//fonts.googleapis.com/css" );
	}

	return $font_url;
}



/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom post format functionality.
 */
require get_template_directory() . '/inc/post-formats.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
