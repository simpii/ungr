<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
        <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,700&subset=latin,latin-ext" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="/new/dist/css/michal-ungr.css">

    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
  <![endif]-->
</head>
<?php if (!is_single()){ ?>
<body <?php body_class(); ?>>
    <a href="/"><img src="/dist/img/logo.png" srcset="/dist/img/logo.png 1x, /dist/img/logo@2x.png 2x" alt="Michal Ungr autorské zahrady" class="main-logo" width="300"></a>
    
    <?php if(!is_front_page()){ 
        $image = get_field('hlavicka_pozadi');
        $imageSrc = wp_get_attachment_image_src( $image, 'subpage-header' );

        ?>
    <div class="page-header" style="background-image: url('<?php print $imageSrc[0]; ?>');">
    <?php } ?>    
    <nav id="site-navigation"  class="main-menu" role="navigation">
        <button class="main-menu-toggle"><?php _e( 'Menu', 'eighties' ); ?> <span class="toggle-icon"></span></button>

        <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
			<?php wp_nav_menu( array( 'theme_location' => 'social', 'menu_class' => 'menu menu-social', 'link_before' => '<span class="screen-reader-text">', 'link_after' => '</span>', 'fallback_cb' => false ) ); ?>
    </nav>
    <?php if(!is_front_page()){ ?>
    </div>
    <?php } ?>

    <?php if(is_front_page()){
        $slider = array();
        for($i=1;$i<=3;$i++){
            if(get_field('slide_'.$i)){
            $image = get_field('slide_'.$i);
            $imageSrc = wp_get_attachment_image_src( $image, 'main-slider' );
            $slider[] = $imageSrc[0];
            }
        }  
        
        ?>
    <div class="hp-image" >
        <?php if ($slider){ ?>
        <div class="hp-slideshow slick-slider">
            <?php
            foreach($slider as $slide){
                print('<div class="slick-slide" style="background-image: url(\''.$slide.'\');"></div>');
            }
            ?>
        </div>
        <?php } ?>
        <div class="hp-footer">
            <button class="hp-scrorrow" title="Scroll down">Scroll down</button>
            <?php footer_text(); ?>
        </div>
    </div>
    <?php } ?>

		<div id="content" class="page-content">

<?php }else{ ?>
 
                    <body class="subpage-gallery">
                        
                    <a href="/"><img src="/dist/img/logo.png" srcset="/dist/img/logo.png 1x, /dist/img/logo@2x.png 2x" alt="Michal Ungr autorské zahrady" class="main-logo" width="300"></a>
    <nav id="site-navigation"  class="main-menu" role="navigation">
        <button class="main-menu-toggle"><?php _e( 'Menu', 'eighties' ); ?> <span class="toggle-icon"></span></button>

        <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
			<?php wp_nav_menu( array( 'theme_location' => 'social', 'menu_class' => 'menu menu-social', 'link_before' => '<span class="screen-reader-text">', 'link_after' => '</span>', 'fallback_cb' => false ) ); ?>
    </nav>

     
                        
                        
                        
                    
<?php } ?>

                    
