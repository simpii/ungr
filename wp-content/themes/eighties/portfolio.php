<?php
/**
 * Template Name: Portfolio
 *
 * @package Eighties
 * @author Justin Kopepasah
 * @since 1.0.0
 */

get_header(); ?>

	<div id="primary" class="content-area page-wrapper">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>
				<?php
					/**
					 * Since this is the portfolio template, we should
					 * display some projects. We use get posts here,
					 * because we are only retreving a set amount of
					 * projects.
					*/
					$args = array(
						'posts_per_page' => 12,
                                                'orderby'          => 'date',
                                                'order'            => 'DESC',
					);

					$portfolio = get_posts( $args );

					?>
						<div id="portfolio-wrapper" class="list-cells">
							<?php foreach ( $portfolio as $post ) : setup_postdata( $post ); ?>
								<?php get_template_part( 'content', 'portfolio' ); ?>
							<?php endforeach; ?>
						</div>
					<?php

					wp_reset_postdata();
				?>

				

			<?php endwhile; // end of the loop. ?>
                    
                    <div class="list-more">
                <button>více</button>
            </div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
